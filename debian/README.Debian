Debian packages for GemRB
====================================

These packages include all upstream GemRB sources, and can be used to run old
Infinity Engine games natively on your Debian distribution.

The packages do not contain any of the game data, except from the custom files
provided by GemRB upstream sources.


What to do next?
-------------------------------------

GemRB can be used to play to multiple games, but there is a bit of
configuration to do upfront.

Each game comes with its dedicated Debian package, containing the patches for
the game, as well as a specific executable. There is a manpage for gemrb, and
linked manpages for each of the game executables.

When you run GemRB from one of the game executables, it will look for a
configuration file with the same name as the executable in ~/.gemrb/, or use
the configuration file provided with the -c flag on the command-line.

A sample configuration files is also packaged in
/usr/share/doc/<game-package>/examples folder that can be copied to ~/.gemrb/
and ajusted.

The main parameter to change in the provided example configuration files is
the GamePath and CD# paths. These parameters should point to the game
installation path, and the CD paths respectively.

With most digital distributions of Infinity Engine games, the CD paths are
meaningless, and only the CD1 path is required, usually pointing to the data/
subfolder of the game installation.


Installation hints
-------------------------------------

Easy: The game-data-packager Debian package can be used to easily package the
original game data from GoG into installable Debian packages (Currently only
English language is supported).

GoG: The digital distributions available on GoG are only provided in the form
of a win32 setup application. There are multiple tools that can be used to
install the games under Debian:
  - wine: simply run the setup.exe with Wine
  - playonlinux: there are some scripts available for GoG version of the games
  - innoextract: it can extract the game data from the setup.exe directly

Other: Sorry you're on your own, but it shouldn't be that hard.


Can I have higher resolutions?
-------------------------------------

Well, you will need the widescreen path for that, which can be found on
http://gibberlings3.net.

Playonlinux has a script for its installation through Wine, and it should work
with GemRB. Other community patches *may* be installable through Wine directly,
but Linux support is generally scarce.
